import type { RouteRecordRaw } from 'vue-router'
import { RouteName } from '@/common/constants/route-name'

export const routes: RouteRecordRaw[] = [
  {
    path: '/:lineId(\\d+)?',
    name: RouteName.lines,
    component: () => import('@/views/bus-lines/BusLines.vue')
  },
  {
    path: '/stops',
    name: RouteName.stops,
    component: () => import('@/views/stops/BusStops.vue')
  }
]
