import type { AppState } from '@/store/types'

export const state: AppState = {
  stopList: [],
  isLoading: false,
  isError: false,
  selectedLine: null,
  selectedStop: '',
  search: ''
}
