import type { Stop } from '@/common/types/stop'

// export interface StopState {
//   stopList: Stop[]
//   isLoading: boolean
//   isError: boolean
// }

// export interface LineState {
//   selectedLine: number | null
//   selectedStop: string
// }

export interface AppState {
  selectedLine: number | null
  selectedStop: string
  stopList: Stop[]
  isLoading: boolean
  isError: boolean
  search: string
}
