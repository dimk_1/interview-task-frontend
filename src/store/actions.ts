import { ActionTree } from 'vuex'
import { busService } from '@/services/app/bus.service'
import { Actions, Mutations } from '@/store/constants'
import type { AppState } from '@/store/types'

export const actions: ActionTree<AppState, AppState> = {
  async [Actions.PROVIDE_STOP_LIST]({ commit }) {
    commit(Mutations.SET_LOADING_STATE, true)
    commit(Mutations.SET_ERROR_STATE, false)

    // Await 2 seconds to simulate a slow API call
    await new Promise((resolve) => setTimeout(resolve, 2000))

    try {
      // throw new Error('Something went wrong')
      const lineList = await busService.getStopList()
      commit(Mutations.SET_STOP_LIST, lineList)
    } catch (error) {
      commit(Mutations.SET_ERROR_STATE, true)
    } finally {
      commit(Mutations.SET_LOADING_STATE, false)
    }
  },
  [Actions.CLEAR_STOP_LIST]({ commit }) {
    commit(Mutations.SET_STOP_LIST, [])
  },
  [Actions.SELECT_LINE]({ commit }, line: number) {
    commit(Mutations.SET_SELECTED_LINE, line)
    commit(Mutations.SET_SELECTED_STOP, '')
  },
  [Actions.SELECT_STOP]({ commit }, stop: string) {
    commit(Mutations.SET_SELECTED_STOP, stop)
  },
  [Actions.CLEAR_SELECTED_LINE]({ commit }) {
    commit(Mutations.SET_SELECTED_LINE, null)
  },
  [Actions.CLEAR_SELECTED_STOP]({ commit }) {
    commit(Mutations.SET_SELECTED_STOP, '')
  },
  [Actions.UPDATE_SEARCH]({ commit }, search: string) {
    commit(Mutations.SET_SEARCH_STRING, search)
  },
  [Actions.CLEAR_SEARCH]({ commit }) {
    commit(Mutations.SET_SEARCH_STRING, '')
  }
}
