// Getters

export enum Getters {
  GET_LINE_LIST = 'GET_LINE_LIST',
  GET_LINE_STOP_LIST = 'GET_LINE_STOP_LIST',
  GET_SEARCH = 'GET_SEARCH',
  GET_FILTERED_STOPS = 'GET_FILTERED_STOPS',
  GET_STOP_SCHEDULE = 'GET_STOP_SCHEDULE',
  GET_SELECTED_LINE = 'GET_SELECTED_LINE',
  GET_SELECTED_STOP = 'GET_SELECTED_STOP',
  GET_LOADING_STATE = 'GET_STOP_LOADING_STATE',
  GET_ERROR_STATE = 'GET_STOP_ERROR_STATE'
}

// Mutations

export enum Mutations {
  SET_STOP_LIST = 'SET_STOP_LIST',
  SET_SELECTED_LINE = 'SET_SELECTED_LINE',
  SET_SELECTED_STOP = 'SET_SELECTED_STOP',
  SET_LOADING_STATE = 'SET_LOADING_STATE',
  SET_ERROR_STATE = 'SET_ERROR_STATE',
  SET_SEARCH_STRING = 'SET_SEARCH_STRING'
}

// Actions

export enum Actions {
  PROVIDE_STOP_LIST = 'PROVIDE_STOP_LIST',
  SELECT_LINE = 'SELECT_LINE',
  SELECT_STOP = 'SELECT_STOP',
  UPDATE_SEARCH = 'UPDATE_SEARCH',
  CLEAR_STOP_LIST = 'CLEAR_STOP_LIST',
  CLEAR_SELECTED_LINE = 'CLEAR_SELECTED_LINE',
  CLEAR_SELECTED_STOP = 'CLEAR_SELECTED_STOP',
  CLEAR_SEARCH = 'CLEAR_SEARCH'
}

// export enum LineGetters {
//   GET_SELECTED_LINE = 'GET_SELECTED_LINE',
//   GET_SELECTED_STOP = 'GET_SELECTED_STOP'
// }

// Mutations

// export enum StopMutations {
//   SET_STOP_LIST = 'UPDATE_STOP_LIST',
//   SET_STOP_LOADING_STATE = 'SET_STOP_LOADING_STATE',
//   SET_STOP_ERROR_STATE = 'SET_STOP_ERROR_STATE'
// }

// export enum LineMutations {}
// SET_SELECTED_LINE = 'SET_SELECTED_LINE',
// SET_SELECTED_STOP = 'SET_SELECTED_STOP'

// Actions

// export enum StopActions {
//   PROVIDE_STOP_LIST = 'PROVIDE_STOP_LIST'
// }

// export enum LineActions {
//   SELECT_LINE = 'SELECT_LINE',
//   SELECT_STOP = 'SELECT_STOP',
//   CLEAR_SELECTED_LINE = 'CLEAR_SELECTED_LINE',
//   CLEAR_SELECTED_STOP = 'CLEAR_SELECTED_STOP'
// }

// export const STOP_STORE = {
//   GETTERS: StopGetters,
//   MUTATIONS: StopMutations,
//   ACTIONS: StopActions
// }

// export const LINE_STORE = {
//   GETTERS: LineGetters,
//   MUTATIONS: LineMutations,
//   ACTIONS: LineActions
// }
