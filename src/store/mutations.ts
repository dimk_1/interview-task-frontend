import { MutationTree } from 'vuex'
import type { Stop } from '@/common/types/stop'
import { Mutations } from '@/store/constants'
import type { AppState } from './types'

export const mutations: MutationTree<AppState> = {
  [Mutations.SET_STOP_LIST](state: AppState, payload: Stop[]) {
    state.stopList = payload
  },
  [Mutations.SET_SELECTED_LINE](state: AppState, payload: number | null) {
    state.selectedLine = payload
  },
  [Mutations.SET_SELECTED_STOP](state: AppState, payload: string) {
    state.selectedStop = payload
  },
  [Mutations.SET_LOADING_STATE](state: AppState, payload: boolean) {
    state.isLoading = payload
  },
  [Mutations.SET_ERROR_STATE](state: AppState, payload: boolean) {
    state.isError = payload
  },
  [Mutations.SET_SEARCH_STRING](state: AppState, payload: string) {
    state.search = payload
  }
}
