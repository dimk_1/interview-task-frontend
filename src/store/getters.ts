import { GetterTree } from 'vuex'
import type { Stop } from '@/common/types/stop'
import { sortByTime } from '@/common/utils/date-utils'
import { Getters } from '@/store/constants'
import type { AppState } from '@/store/types'

export const getters: GetterTree<AppState, AppState> = {
  [Getters.GET_LINE_LIST]: (state: AppState): number[] => {
    return state.stopList
      .reduce((acc: number[], { line }) => {
        if (!acc.includes(line)) {
          acc.push(line)
        }
        return acc
      }, [])
      .sort((a, b) => a - b)
  },
  [Getters.GET_LINE_STOP_LIST]: (state: AppState): Stop[] => {
    if (!state.selectedLine) {
      return []
    }

    return state.stopList
      .filter(({ line }) => line === state.selectedLine)
      .map((stop) => stop)
      .reduce((acc: Stop[], stop) => {
        if (!acc.find(({ stop: accStop }) => accStop === stop.stop)) {
          acc.push(stop)
        }
        return acc
      }, [])
      .sort((a, b) => a.order - b.order)
  },
  [Getters.GET_STOP_SCHEDULE]: (state: AppState): Stop[] => {
    if (!state.selectedLine || !state.selectedStop) {
      return []
    }

    return sortByTime<Stop>(
      state.stopList.filter(({ line, stop }) => line === state.selectedLine && stop === state.selectedStop)
    )
  },
  [Getters.GET_FILTERED_STOPS]: (state: AppState): Stop[] => {
    return state.stopList
      .reduce((acc: Stop[], stop) => {
        if (!acc.find(({ stop: accStop }) => accStop === stop.stop)) {
          acc.push(stop)
        }
        return acc
      }, [])
      .filter(({ stop }) => stop.toLowerCase().includes(state.search.toLowerCase()))
      .sort((a, b) => a.order - b.order)
  },
  [Getters.GET_LOADING_STATE]: (state: AppState): boolean => state.isLoading,
  [Getters.GET_ERROR_STATE]: (state: AppState): boolean => state.isError,
  [Getters.GET_SELECTED_LINE]: (state: AppState): number | null => state.selectedLine,
  [Getters.GET_SELECTED_STOP]: (state: AppState): string => state.selectedStop,
  [Getters.GET_SEARCH]: (state: AppState): string => state.search
}
