import { type RouteName } from '@/common/constants/route-name'

export interface NavigationItem {
  label: string
  routeName: RouteName
}
