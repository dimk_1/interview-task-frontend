class ApiUtils {
  apiGatewayUrl: string

  constructor() {
    // TODO: add logic based on environment variables to determine the API gateway URL
    this.apiGatewayUrl = 'http://localhost:3000'
  }

  getPublicApiUrl(url = ''): string {
    return url ? `${this.apiGatewayUrl}/${url}` : this.apiGatewayUrl
  }
}

export const apiUtils = new ApiUtils()
