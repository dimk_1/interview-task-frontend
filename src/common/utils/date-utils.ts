export const sortByTime = <T extends { time: string }>(list: T[]) =>
  list.sort((a, b) => {
    // Split the time string into hours and minutes and convert them to numbers
    const [hoursA, minutesA] = a.time.split(':').map(Number)
    const [hoursB, minutesB] = b.time.split(':').map(Number)

    // Convert hours and minutes to total minutes since midnight
    const totalMinutesA = hoursA * 60 + minutesA
    const totalMinutesB = hoursB * 60 + minutesB

    // Compare these total minutes to sort
    return totalMinutesA - totalMinutesB
  })
