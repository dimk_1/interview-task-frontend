import { computed } from 'vue'
import { useStore } from 'vuex'

import { Actions, Getters } from '@/store/constants'

export const useBus = () => {
  const store = useStore()

  const lineList = computed<number[]>(() => store.getters[Getters.GET_LINE_LIST])

  const isLoading = computed<boolean>(() => store.getters[Getters.GET_LOADING_STATE])
  const isError = computed<boolean>(() => store.getters[Getters.GET_ERROR_STATE])

  const provideStopList = () => store.dispatch(Actions.PROVIDE_STOP_LIST)

  return {
    isLoading,
    isError,
    lineList,
    provideStopList
  }
}
