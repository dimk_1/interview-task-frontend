import { computed } from 'vue'
import { useStore } from 'vuex'
import type { Stop } from '@/common/types/stop'

import { Actions, Getters } from '@/store/constants'

export const useBusStops = () => {
  const store = useStore()

  const filteredStops = computed<Stop[]>(() => store.getters[Getters.GET_FILTERED_STOPS])

  const searchModel = computed({
    get: () => store.getters[Getters.GET_SEARCH],
    set: (value: string) => store.dispatch(Actions.UPDATE_SEARCH, value)
  })

  const clearAll = () => {
    store.dispatch(Actions.UPDATE_SEARCH, '')
    store.dispatch(Actions.CLEAR_STOP_LIST)
  }

  return {
    searchModel,
    filteredStops,
    clearAll
  }
}
