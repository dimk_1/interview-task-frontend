import { computed } from 'vue'
import { useStore } from 'vuex'
import type { Stop } from '@/common/types/stop'

import { Actions, Getters } from '@/store/constants'

export const useBusLines = () => {
  const store = useStore()

  const lineStopList = computed<Stop[]>(() => store.getters[Getters.GET_LINE_STOP_LIST])
  const stopSchedule = computed<Stop[]>(() => store.getters[Getters.GET_STOP_SCHEDULE])

  const selectedLine = computed<number | null>(() => store.getters[Getters.GET_SELECTED_LINE])
  const selectedStop = computed<string>(() => store.getters[Getters.GET_SELECTED_STOP])

  const setSelectedLine = (line: number) => store.dispatch(Actions.SELECT_LINE, line)
  const setSelectedStop = (stop: string) => store.dispatch(Actions.SELECT_STOP, stop)

  const clearStopList = () => store.dispatch(Actions.CLEAR_STOP_LIST)
  const clearSelectedLine = () => store.dispatch(Actions.CLEAR_SELECTED_LINE)
  const clearSelectedStop = () => store.dispatch(Actions.CLEAR_SELECTED_STOP)

  const clearAll = () => {
    clearStopList()
    clearSelectedLine()
    clearSelectedStop()
  }

  return {
    lineStopList,
    stopSchedule,
    selectedLine,
    selectedStop,
    setSelectedLine,
    setSelectedStop,
    clearStopList,
    clearSelectedLine,
    clearSelectedStop,
    clearAll
  }
}
