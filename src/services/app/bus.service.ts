import { apiClient } from '@/services/api-client'
import { apiUtils } from '@/common/utils/api-utils'

import { type Stop } from '@/common/types/stop'

class BusService {
  async getStopList() {
    const { data } = await apiClient.get<Stop[]>(apiUtils.getPublicApiUrl('stops'))
    return data
  }
}

export const busService = new BusService()
